<?
/**
La encargada de:
- Conectar con la base de datos
- Determinar que método debe de utilizar para resolver la petición, a partir de la url de dicha petición.
- Utilizar los métodos heredados de la clase padre para sanear los datos de entrada de la petición y devolver el resultado al cliente.
*/
require_once("rest.php");
require_once("../includes/config/configuration.php");
require_once("../includes/objects/base/class.database.php");
require_once("../includes/objects/class.usuario.php");
class usuarioApi extends rest {

  private function devolverError($id) {
    $errores = array(
		array('estado' => "error", "msg" => "petición no encontrada"),
		array('estado' => "error", "msg" => "petición no aceptada"),
		array('estado' => "error", "msg" => "petición sin contenido"),
		array('estado' => "error", "msg" => "email o password incorrectos"),
		array('estado' => "error", "msg" => "token vacío"),
		array('estado' => "error", "msg" => "token no válido"),
		array('estado' => "error", "msg" => "nombre de usuario en uso, selecciona otro"),
		array('estado' => "error", "msg" => "faltan campos"),
		array('estado' => "error", "msg" => "Se ha producido un error al actualizar a tu compañero, intentalo de nuevo más tarde"),
		array('estado' => "error", "msg" => "Error desconocido")
    );
    return $errores[$id];
  }
	public function procesarLLamada() {
		file_put_contents("log.log", "POST: ".print_r($_POST, true)."\n\nRequest: ".print_r($_REQUEST, true)."\n\nPUT: ".file_get_contents("php://input"));
		if (isset($_REQUEST['mg'])) {
			//si por ejemplo pasamos explode('/','////controller///method/.///args///') el resultado es un array con elem vacios;
			//Array ( [0] => [1] => [2] => [3] => [4] => controller [5] => [6] => [7] => method [8] => [9] => [10] => [11] => args [12] => [13] => [14] => )
			$url = explode('/', trim($_REQUEST['mg']));
			//con array_filter() filtramos elementos de un array pasando función callback, que es opcional.
			//si no le pasamos función callback, los elementos false o vacios del array serán borrados
			//por lo tanto la entre la anterior función (explode) y esta eliminamos los '/' sobrantes de la URL
			$url = array_filter($url);
			$this->_metodo = strtolower(array_shift($url));
			$this->_argumentos = $url;
			$func = $this->_metodo;
			if ((int) method_exists($this, $func) > 0) {
				if (count($this->_argumentos) > 0) {
					call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
				} else {//si no lo llamamos sin argumentos, al metodo del controlador
					call_user_func(array($this, $this->_metodo));
				}
			}else {
				$this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
			}
		}
		$this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
	}
	private function convertirJson($data) {
		return json_encode($data);
	}

	private function login() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		if (isset($this->datosPeticion['us'], $this->datosPeticion['pw'])) {
			$us = $this->datosPeticion['us'];
			$pw = $this->datosPeticion['pw'];

			$usuario = new usuario();
			$fcv_array = array(array("username", "=", $us), array("passwd", "=", $pw));

			$usuario = $usuario->GetList($fcv_array);
			if(count($usuario) <= 0) {
				$this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 200);
			} else {
				$usuario = $usuario[0];
				// Creamos un loginToken a partir del nombre de usuario
				require_once("utils.php");
				require_once("../includes/objects/class.grupo.php");
				require_once("../includes/objects/class.partner.php");
				$lt = utils::generateFormHash($usuario->username);
				$usuario->loginToken = $lt;
				$usuario->fechaUltimoAcceso = @date().time();
				$usuario->Save();
				
				$grupoName = $usuario->getGroupName();
				$compa = $usuario->getCompa();

				$respuesta['estado'] = 'correcto';
				$respuesta['msg'] = 'datos pertenecen a usuario registrado';
				$respuesta['usuario']['nombre'] = $usuario->nombre;
				$respuesta['usuario']['apellidos'] = $usuario->apellidos;
				$respuesta['usuario']['email'] = $usuario->email;
				$respuesta['usuario']['desayunoComida'] = $usuario->desayunoComida;
				$respuesta['usuario']['desayunoBebida'] = $usuario->desayunoBebida;
				$respuesta['usuario']['username'] = $usuario->username;
				$respuesta['usuario']['grupoId'] = $usuario->grupoId;
				$respuesta['usuario']['grupo'] = $grupoName; // Enviar el nombre del grupo
				$respuesta['usuario']['actualPartnerId'] = $usuario->actualPartnerId;
				$respuesta['usuario']['actualPartnerName'] = $compa->nombre . " " . $compa->apellidos;
				$respuesta['usuario']['voy'] = $usuario->voy;
				$respuesta['usuario']['lt'] = $lt;

				$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
			}
		}
		$this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
	}

	private function user() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();

		require_once("../includes/objects/class.grupo.php");
		require_once("../includes/objects/class.partner.php");

		$grupoName = $usuario->getGroupName();
		$compa = $usuario->getCompa();

		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'datos pertenecen a usuario registrado';
		$respuesta['usuario']['nombre'] = $usuario->nombre;
		$respuesta['usuario']['apellidos'] = $usuario->apellidos;
		$respuesta['usuario']['email'] = $usuario->email;
		$respuesta['usuario']['desayunoComida'] = $usuario->desayunoComida;
		$respuesta['usuario']['desayunoBebida'] = $usuario->desayunoBebida;
		$respuesta['usuario']['username'] = $usuario->username;
		$respuesta['usuario']['grupoId'] = $usuario->grupoId;
		$respuesta['usuario']['grupo'] = $grupoName; // Enviar el nombre del grupo
		$respuesta['usuario']['actualPartnerId'] = $usuario->actualPartnerId;
		$respuesta['usuario']['actualPartnerName'] = $compa->nombre . " " . $compa->apellidos;
		$respuesta['usuario']['voy'] = $usuario->voy;
		$respuesta['usuario']['lt'] = $lt;

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
	}

	private function mypartner() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$lt = $this->datosPeticion['lt'];
		if (isset($lt)) {
			//Comprobamos si el token es válido:
			$usuario = usuario::getUserFromToken($lt);
			if(count($usuario) > 0) {
				// Token Ok:
				$usuario = $usuario[0];

				require_once("../includes/objects/class.partner.php");
				$compa = $usuario->getCompa();

				$respuesta['estado'] = 'correcto';
				$respuesta['msg'] = 'compañero de desayuno';
				$respuesta['usuario']['nombre'] = $compa->nombre;
				$respuesta['usuario']['apellidos'] = $compa->apellidos;
				$respuesta['usuario']['email'] = $compa->email;
				$respuesta['usuario']['desayunoComida'] = $compa->desayunoComida;
				$respuesta['usuario']['desayunoBebida'] = $compa->desayunoBebida;
				$respuesta['usuario']['username'] = $compa->username;

				$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
			} else {
				$this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 401);
			}
		} else {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(4)), 401);
		}
	}

	private function resumen() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();
		require_once("../includes/objects/class.partner.php");
		$resumen = $usuario->getResumenDesayunos();
		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'Resumen de desayuno';
		$respuesta['resumen'] = $resumen;

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
	}

	private function register() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}

		require_once("utils.php");
		$lt = utils::generateFormHash($usuario->username);

		$username = $this->datosPeticion['username'];
		$passwd = $this->datosPeticion['passwd'];
		$nombre = $this->datosPeticion['nombre'];
		$apellidos = $this->datosPeticion['apellidos'];
		$email = $this->datosPeticion['email'];
		$desayunoComida = $this->datosPeticion['desayunoComida'];
		$desayunoBebida = $this->datosPeticion['desayunoBebida'];
		$usuario = new usuario();

		if(empty($username) || empty($passwd) || empty($email)) {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(7)), 400);
		}
		if(count($usuario->GetList(array(array(usuario::$fieldUsername, "=", $username)))) > 0) {
			$error['estado'] = 'error';
			$error['msg'] = 'nombre de usuario en uso, selecciona otro';
			$this->mostrarRespuesta($this->convertirJson($error), 200);
			return;
		}

		$usuario = new usuario();
		$usuario->nombre = $nombre;
		$usuario->apellidos = $apellidos;
		$usuario->username = $username;
		$usuario->passwd = $passwd;
		$usuario->email = $email;
		$usuario->desayunoComida = $desayunoComida;
		$usuario->desayunoBebida = $desayunoBebida;
		$usuario->loginToken = $lt;
		$usuario->fechaUltimoAcceso = @date().time();
		$usuario->Save();

		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'Usuario registrado correctamente';
		$respuesta['usuario']['nombre'] = $usuario->nombre;
		$respuesta['usuario']['apellidos'] = $usuario->apellidos;
		$respuesta['usuario']['email'] = $usuario->email;
		$respuesta['usuario']['desayunoComida'] = $usuario->desayunoComida;
		$respuesta['usuario']['desayunoBebida'] = $usuario->desayunoBebida;
		$respuesta['usuario']['username'] = $usuario->username;
		$respuesta['usuario']['grupoId'] = $usuario->grupoId;
		$respuesta['usuario']['grupo'] = $grupoName; // Enviar el nombre del grupo
		$respuesta['usuario']['actualPartnerId'] = $usuario->actualPartnerId;
		$respuesta['usuario']['voy'] = $usuario->voy;
		$respuesta['usuario']['lt'] = $lt;

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
	}

	private function updateDesayuno(){
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();
		require_once("../includes/objects/class.partner.php");
		$desayunoComida = $this->datosPeticion['desayunoComida'];
		$desayunoBebida = $this->datosPeticion['desayunoBebida'];
		if(empty($desayunoComida) || empty($desayunoBebida)) {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(7)), 400);
		}

		$usuario->updateDesayuno($desayunoComida, $desayunoBebida);
		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'Desayuno actualizado correctamente';
		$respuesta['usuario']['desayunoComida'] = $usuario->desayunoComida;
		$respuesta['usuario']['desayunoBebida'] = $usuario->desayunoBebida;
		$respuesta['usuario']['voy'] = $usuario->voy;

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);

	}

	/**
	Compartir el desayuno con un compañero
	*/
	private function compartir() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();
		// TODO
		$idCompa = $this->datosPeticion['idCompa'];

		$userUpdated = $usuario->compartir($idCompa);
		if(empty($userUpdated->usuarioId)) {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(8)), 205);
		} else {
				require_once("../includes/objects/class.grupo.php");
				require_once("../includes/objects/class.partner.php");

				$grupoName = $userUpdated->getGroupName();
				$compa = $userUpdated->getCompa();


				$respuesta['estado'] = 'correcto';
				$respuesta['msg'] = 'Ok';
				$respuesta['usuario']['actualPartnerId'] = $userUpdated->actualPartnerId;
				$respuesta['usuario']['actualPartnerName'] = $compa->nombre . " " . $compa->apellidos;

				$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
		}
	}

	/**
	Marcar al usuario autenticado como que va a desayunar
	*/
	public function goBreakfast() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();

		require_once("../includes/objects/class.partner.php");
		$idCompa = $this->datosPeticion['idCompa'];
		$ret = $usuario->goBreakfast($idCompa);

		if($ret == 1) {
			// Todo funciona OK
			$respuesta['estado'] = 'correcto';
			$respuesta['msg'] = 'Ok';
			$respuesta['usuario']['voy'] = 1;

			$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
		} else {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(8)), 205);
		}
	}

	/**
	Devuelve el listado de usuarios apuntados al grupo al que pertenece el usuario autenticado
	*/
	private function groupUsers() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();
		$allGroupUsers = $usuario->getAllBreakfastGroup(false);

		if(count($allGroupUsers) >= 0) {
			$respuesta['estado'] = 'correcto';
			$respuesta['msg'] = 'Ok';
			$respuesta['grupo'] = $allGroupUsers;

			$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
		}
	}

	/**
	Devuelve el listado de grupos que existe en la base de datos.
	*/
	private function groups() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		require_once("../includes/objects/class.grupo.php");
		$allGroups = grupo::getAllGroups();

		if(count($allGroups) >= 0) {
			$respuesta['estado'] = 'correcto';
			$respuesta['msg'] = 'Ok';
			$respuesta['grupos'] = $allGroups;

			$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
		}
	}

	/**
	Apuntar al usuario autenticado al grupo que marque
	*/
	private function updateGrupo() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		require_once("../includes/objects/class.grupo.php");

		$usuario = $this->compruebaToken();
		$idGrupo = $this->datosPeticion['grupoId'];
		$ret = $usuario->updateGrupo($idGrupo);
		
		if(!empty($ret->usuarioId)) {
			$grupoName = $ret->getGroupName();

			$respuesta['estado'] = 'correcto';
			$respuesta['msg'] = 'Ok';
			$respuesta['usuario']['grupoId'] = $ret->grupoId;
			$respuesta['usuario']['grupo'] = $grupoName; // Enviar el nombre del grupo

			$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
		} else {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(9)), 501);
		}
	}

	private function createBreakfastGroup() {
		if ($_SERVER['REQUEST_METHOD'] != "POST") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		$usuario = $this->compruebaToken();
		require_once("../includes/objects/class.grupo.php");
		$nombreGrupo = $this->datosPeticion['nombreGrupo'];
		if(empty($nombreGrupo)) {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(10)), 400);
		}

		grupo::createGrupo($nombreGrupo, $usuario->email);
		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'Ok';

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
	}

	private function searchBreakfastGroup() {
		if ($_SERVER['REQUEST_METHOD'] != "GET") {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
		}
		//$usuario = $this->compruebaToken();
		require_once("../includes/objects/class.grupo.php");
		$nombreGrupo = utf8_decode($this->datosPeticion['nombreGrupo']);
		$emailAdmin = $this->datosPeticion['emailAdmin'];
		$allGroups = grupo::getGroups($nombreGrupo, $emailAdmin);

		$respuesta['estado'] = 'correcto';
		$respuesta['msg'] = 'Ok';
		$respuesta['grupos'] = $allGroups;

		$this->mostrarRespuesta($this->convertirJson($respuesta), 200);
	}

	private function compruebaToken() {
		$lt = $_GET['lt'];
		if (isset($lt)) {
			//Comprobamos si el token es válido:
			$usuario = usuario::getUserFromToken($lt);
			if(count($usuario) > 0) {
				// Token Ok:
				return $usuario[0];
			} else {
				$this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 401);
			}
		} else {
			$this->mostrarRespuesta($this->convertirJson($this->devolverError(4)), 401);
		}
		$this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
	}

}
$usuarioApi = new usuarioApi();
$usuarioApi->procesarLLamada();

?>