<?
/**
La encargada de:
- Conectar con la base de datos
- Determinar que método debe de utilizar para resolver la petición, a partir de la url de dicha petición.
- Utilizar los métodos heredados de la clase padre para sanear los datos de entrada de la petición y devolver el resultado al cliente.
*/
require_once("rest.php");
class api extends rest {

  private function devolverError($id) {
    $errores = array(
     array('estado' => "error", "msg" => "petición no encontrada"),
     array('estado' => "error", "msg" => "petición no aceptada"),
     array('estado' => "error", "msg" => "petición sin contenido"),
     array('estado' => "error", "msg" => "email o password incorrectos"),
     array('estado' => "error", "msg" => "error borrando usuario"),
     array('estado' => "error", "msg" => "error actualizando nombre de usuario"),
     array('estado' => "error", "msg" => "error buscando usuario por email"),
     array('estado' => "error", "msg" => "error creando usuario"),
     array('estado' => "error", "msg" => "usuario ya existe")
    );
    return $errores[$id];
  }
   public function procesarLLamada() {
     if (isset($_REQUEST['url'])) {
       //si por ejemplo pasamos explode('/','////controller///method////args///') el resultado es un array con elem vacios;
       //Array ( [0] => [1] => [2] => [3] => [4] => controller [5] => [6] => [7] => method [8] => [9] => [10] => [11] => args [12] => [13] => [14] => )
       $url = explode('/', trim($_REQUEST['url']));
       //con array_filter() filtramos elementos de un array pasando función callback, que es opcional.
       //si no le pasamos función callback, los elementos false o vacios del array serán borrados
       //por lo tanto la entre la anterior función (explode) y esta eliminamos los '/' sobrantes de la URL
       $url = array_filter($url);
       $this->_metodo = strtolower(array_shift($url));
       $this->_argumentos = $url;
       $func = $this->_metodo;
       if ((int) method_exists($this, $func) > 0) {
         if (count($this->_argumentos) > 0) {
           call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
         } else {//si no lo llamamos sin argumentos, al metodo del controlador
           call_user_func(array($this, $this->_metodo));
         }
       }
       else
         $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
     }
     $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
   }
  private function convertirJson($data) {
   return json_encode($data);
  }
}
$api = new api();
$api->procesarLLamada();

?>