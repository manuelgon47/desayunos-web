<?php
include("../includes/includes.inc.php");
$error = array();

$action = $_POST["action"];
if($action == "login") {
	$us = $_POST["us"];
	$pw = $_POST["pw"];
	if(!isset($us) || empty($us) || !isset($pw) || empty($pw)) {
		header('Location: ' . $_SERVER['HTTP_REFERER'] . "?error=1");
	}
	$usuario = new usuario();
	$fcv_array = array(array(usuario::$fieldUsername, "=", $us), array(usuario::$fieldPasswd, "=", md5($pw)));
	$usuario = $usuario->GetList($fcv_array);
	if(count($usuario) <= 0) {
		header('Location: ' . $_SERVER['HTTP_REFERER'] . "?error=2");
	} else {
		$usuario = $usuario[0];
		if(empty($usuario->usuariosId)){
			header('Location: ' . $_SERVER['HTTP_REFERER'] . "?error=3");
		}

		// Login OK:
		$_SESSION['username'] = $usuario->username;
		header("Location: ../desayuno/index.php");
	}
} else if($action == "register") {
		global $configuration;
		$username = $_POST["username"];
		$passwd = $_POST["passwd"];
		$nombre = $_POST["nombre"];
		$apellidos = $_POST["apellidos"];
		$email = $_POST["email"];
		$desayunoComida = $_POST["desayunoComida"];
		$desayunoBebida = $_POST["desayunoBebida"];

		$usuario = new usuario();

		if(empty($username)) {
			header("Location: " . $configuration["homepage"] . "/login/register.php?error=El campo usuario es obligatorio");
		}
		// Compruebo si el nombre de usuario ya está usado:
		$fcv_array = array(array(usuario::$fieldUsername, "=", $username));
		$usuario = $usuario->GetList($fcv_array);
		if(count($usuario) > 0) {
			return header("Location: " . $configuration["homepage"] . "/login/register.php?error=Este nombre de usuario ya está siendo usado, elige otro.");
		}
		if(empty($passwd)) {
			return header("Location: " . $configuration["homepage"] . "/login/register.php?error=El campo contraseña es obligatorio");
		} if(strlen($passwd) < 6) {
			return header("Location: " . $configuration["homepage"] . "/login/register.php?error=El campo contraseña es debe tener al menos 6 dígitos");
		}
		if(empty($nombre)) {
			return header("Location: " . $configuration["homepage"] . "/login/register.php?error=El campo nombre es obligatorio");
		}
		if(empty($email)) {
			return header("Location: " . $configuration["homepage"] . "/login/register.php?error=El campo email es obligatorio");
		}

		$usuario = new usuario();
		$usuario->nombre = $nombre;
		$usuario->apellidos = $apellidos;
		$usuario->username = $username;
		$usuario->passwd = md5($passwd);
		$usuario->email = $email;
		$usuario->desayunoComida = $desayunoComida;
		$usuario->desayunoBebida = $desayunoBebida;
		$usuario->Save();

}

?> 