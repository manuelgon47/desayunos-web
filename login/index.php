<?php
include("../includes/principal.inc.php");
$username = $_SESSION["username"];

if(!empty($username)) {
	header("Location: ".$configuration["homepage"]."/desayuno");
}
imprimeCabecera();
?>
	<div class="wrapperLogin">
		<div class="form-login-header">
			<h1>Autentícate</h1>
			<span>Si ya está registrado, introduzca su nombre de usuario y contraseña para acceder.</span>
		</div>
		<form class="loginForm" action="controller.php" name="login" id="login" method="post">
			<input name="us" type="text" value="" onfocus="this.value=''" />
			<input name="pw" type="password" value="" onfocus="this.value=''" />

	    	<input type="submit" name="action" value="login" class="button" />
	    	<!--<input type="submit" name="action" value="register" class="register" />-->
		</form>
		<a href="register.php">register</a>
	</div>
<?php
imprimePie();
?>