<?php
/**
* @author Manuel González Villegas
*/
include_once('base/class.grupoBase.php');
class grupo extends grupoBase {
	
	static $fieldGrupoId = "grupoid";
	static $fieldNombre = "nombre";
	static $fieldFechaUltimoResumen = "fechaultimoresumen";
	static $fieldAdmingrupo = "admingrupo";

	function toString() {
		$ret["grupoid"] = $this->grupoId;
		$ret["nombre"] = $this->nombre;
		$ret["adminGrupo"] = $this->admingrupo;
		return $ret;
	}

	static function createGrupo($nombre, $admingrupo) {
		if(!isset($nombre) || empty($nombre) || !isset($admingrupo) || empty($admingrupo)) {
			return false;
		}
		$newGroup = new grupo();
		$newGroup->nombre = $nombre;
		$newGroup->admingrupo = $admingrupo;
		$newGroup->Save();
	}

	static function getGroups($nombreGrupo = "", $emailAdmin = "") {
		if(!empty($nombreGrupo) || (empty($nombreGrupo) && empty($emailAdmin))) {
			$nombreGrupo = "%".$nombreGrupo."%";
		}
		if(!empty($emailAdmin)) {
			$emailAdmin = "%".$emailAdmin."%";
		}
		$grupo = new grupo();
		$fvc_array = array(array(grupo::$fieldNombre, "like", $nombreGrupo));
		$fvc_array_1 = array(array(grupo::$fieldAdmingrupo, "like", $emailAdmin));
		$ret = array();
		$filteredGroups = $grupo->GetList($fvc_array);
		$filteredGroups_1 = $grupo->GetList($fvc_array_1);

		foreach ($filteredGroups as $g) {
			$ret[] = $g->toString();
		}

		foreach ($filteredGroups_1 as $g) {
			$ret[] = $g->toString();
		}

		return $ret;
	}

	static function getAllGroups() {
		$grupo = new grupo();
		$fvc_array = array(array());
		$ret = array();
		foreach ($grupo->GetList() as $g) {
			$ret[] = $g->toString();
		}

		return $ret;
	}

}
?>