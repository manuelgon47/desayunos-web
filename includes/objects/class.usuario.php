<?php
/**
* @author Manuel González Villegas
*/
include_once('base/class.usuarioBase.php');
class usuario extends usuarioBase {

	static $fieldUsuarioId = "usuarioid";
	static $fieldNombre = "nombre";
	static $fieldApellidos = "apellidos";
	static $fieldEmail = "email";
	static $fieldDesayunoComida = "desayunocomida";
	static $fieldDesayunoBebida = "desayunobebida";
	static $fieldUsername = "username";
	static $fieldPasswd = "passwd";
	static $fieldGrupoId = "grupoid";
	static $fieldActualPartnerId = "actualpartnerid";
	static $fieldLoginToken = "logintoken";
	static $fieldFechaUltimoAcceso = "fechaultimoacceso";
	static $fieldVoy = "voy";

	function getMyPartnersGroup() {
		$idGrupo = $this->grupoId;
		if(empty($idGrupo)) {
			return array();
		}

		$fvc_array = array(array(usuario::$fieldGrupoId, "=", $idGrupo), array(usuario::$fieldUsuarioId, "!=", $this->usuarioId));
		return $this->GetList($fvc_array);
	}

	function getBreakfastPartner() {
		$idPartner = $this->actualPartnerId;
		if($idPartner < 0) {
			return new usuario();
		}
		$partner = new partner();
		$partner = $partner->Get($idPartner);
		if(empty($partner->partnerId)) {
			exit("Error al recuperar la pareja a partir del id $idPartner");
		}

		$compa = new usuario();
		if($this->usuarioId != $partner->idUsuario1) {
			$compa = $compa->Get($partner->idUsuario1);
		} else {
			$compa = $compa->Get($partner->idUsuario2);
		}

		return $compa;
	}

	function getAllBreakfastGroup($includeAuthenticatedUser = true) {
		$idGrupo = $this->grupoId;
		if(empty($idGrupo)) {
			return array();
		}
		$fvc_array = array(array(usuario::$fieldGrupoId, "=", $idGrupo));
		$aux = $this->GetList($fvc_array);
		$ret = array();
		foreach ($aux as $usuario) {
			if($usuario->usuarioId != $this->usuarioId || $includeAuthenticatedUser) {
				$ret[] = $usuario->toString();
			}
		}
		return $ret;
	}

	function getGoBreakfastUsersGroup($includeAuthenticatedUser = false) {
		$idGrupo = $this->grupoId;
		if(empty($idGrupo)) {
			return array();
		}
		$fvc_array = array(array(usuario::$fieldGrupoId, "=", $idGrupo), array(usuario::$fieldVoy, "=", 1));
		$aux = $this->GetList($fvc_array);
		$ret = array();
		foreach ($aux as $usuario) {
			if($usuario->usuarioId != $this->usuarioId || $includeAuthenticatedUser) {
				$ret[] = $usuario->toString();
			}
		}
		return $ret;
	}

	/**
	Obtenemos el resumen del pedido
	*/
	function getResumenDesayunos() {
		//$usuariosGrupo = $this->getAllBreakfastGroup();
		$usuariosGrupo = $this->getGoBreakfastUsersGroup(true);
		// Id's de usuarios procesados
		$usuariosId = array();
		$bebidas = array();
		$tostadas = array();
		$pedidoIndividual = $this->getPedidoIndividual($usuariosGrupo);
		//print_r("<pre>".print_r($usuariosGrupo, true)."</pre>");exit();
		foreach ($usuariosGrupo as $usuario) {
			// Comprobamos si tiene compañero:
			if(!in_array($usuario["usuarioId"], $usuariosId)){
				if(!empty($usuario["actualPartnerId"])) {
					$partner = new partner();
					$partner = $partner->Get($usuario["actualPartnerId"]);
					$idCompa = ($partner->idUsuario1 == $usuario["usuarioId"]) ? $partner->idUsuario2 : $partner->idUsuario1;
					//print_r("<pre>".print_r($idCompa, true)."</pre>");exit();
					$compa = new usuario();
					$compa = $compa->Get($idCompa);
					$bebidas[] = $compa->desayunoBebida;
					$usuariosId[] = $compa->usuarioId;
				}
				$tostadas[] = $usuario["desayunoComida"];
				$bebidas[] = $usuario["desayunoBebida"];
				$usuariosId[] = $usuario["usuarioId"];
			}
		}
		return array($bebidas, $tostadas, $pedidoIndividual);
	}

	/**
	Pedido de cada uno
	*/
	function getPedidoIndividual($usuariosGrupo = array()) {
		$ret = array();
		$usuariosId = array();
		foreach ($usuariosGrupo as $usuario) {
			if(!in_array($usuario["usuarioId"], $usuariosId)) {
				if(!empty($usuario["actualPartnerId"])) {
					$partner = new partner();
					$partner = $partner->Get($usuario["actualPartnerId"]);
					$idCompa = ($partner->idUsuario1 == $usuario["usuarioId"]) ? $partner->idUsuario2 : $partner->idUsuario1;
					$compa = new usuario();
					$compa = $compa->Get($idCompa);
					$compa = $compa->toString();
					$compa["comparteCon"] = $usuario["nombre"];
					$ret[] = $compa;
					$usuariosId[] = $idCompa;
					$usuario["comparteCon"] = $compa["nombre"];
				}
				$ret[] = $usuario;
				$usuariosId[] = $usuario["usuarioId"];
			}
		}
		return $ret;
	}

	function getCompa() {
		if($this->actualPartnerId <= 0) {
			return new usuario();
		}
		$partner = new partner();
		$partner = $partner->Get($this->actualPartnerId);
		$idCompa = ($partner->idUsuario1 == $this->usuarioId) ? $partner->idUsuario2 : $partner->idUsuario1;
		$compa = new usuario();
		return $compa->Get($idCompa);
	}

	function updateDesayuno($desayunoComida, $desayunoBebida) {
		if(!isset($desayunoComida) || !isset($desayunoBebida)) {
			exit("No se han pasado los parámetros desayunoComida y desayunoBebida");
		}
		if($this->actualPartnerId > 0) {
			$compa = $this->getCompa();
			if($compa->usuarioId > 0) {
				$compa->desayunoComida = $desayunoComida;
				$compa->Save();
			}
		}

		$this->desayunoComida = $desayunoComida;
		$this->desayunoBebida = $desayunoBebida;
		$this->Save();

		return $this;
	}

	function compartir($idCompa) {
		include_once('class.partner.php');
		if(!isset($idCompa)) {
			exit("No se he indicado el id del usuario con el que se quiere compartir");
		}
		$usuarioCompa = new usuario();
		$usuarioCompa = $usuarioCompa->Get($idCompa);

		// Current user:
		$currentUser = $this;

		if(empty($usuarioCompa->usuarioId)) {
			exit("El id de usuario $idCompa no coincide con ningún id de usuario");
		}

		// Comprobamos que no existe una fila en la tabla partner en la que ya haya compartido con el el desayuno antes
		$partner = new partner();
		$fvc_array1 = array(array(partner::$fieldIdUsuario1, "=", $idCompa), array(partner::$fieldIdUsuario2, "=", $currentUser->usuarioId));
		$fvc_array2 = array(array(partner::$fieldIdUsuario1, "=", $currentUser->usuarioId), array(partner::$fieldIdUsuario2, "=", $idCompa));
		$partner = $partner->GetList($fvc_array1);
		if(count($partner) <= 0) {
			$partner = new partner();
			$partner = $partner->GetList($fvc_array2);
		}

		if(count($partner) <= 0) {
			// No existe esta pareja, la creamos
			$partner = new partner();
			$partner->idUsuario1 = $currentUser->usuarioId;
			$partner->idUsuario2 = $idCompa;
			$partner->idUsuarioUltimoPagar = $currentUser->usuarioId;
			$partner->Save();
		} else {
			$partner = $partner[0];
		}
		// Si ya existe, simplemente metemos el id en nuestro actualPartnerId y en el de nuestro compañero
		$currentUser->actualPartnerId=$partner->partnerId;
		$currentUser->Save();

		$usuarioCompa->actualPartnerId=$partner->partnerId;
		// Seteamos el desayuno del compañero para que coincida con el del usuario actual:
		$usuarioCompa->desayunoComida = $currentUser->desayunoComida;
		$usuarioCompa->Save();

		return $currentUser;
	}

	function getGroupName() {
		$idGrupo = $this->grupoId;
		if($idGrupo > 0) {
			$grupo = new grupo();
			$grupo = $grupo->Get($idGrupo);
			return $grupo->nombre;
		} else {
			return "";
		}
	}

	function goBreakfast($idPartner) {
		// Si se indica el idPartner, marcamos que este usuario también va a desayunar
		if(isset($idPartner) && !empty($idPartner)) {
			$partner = new partner();
			$partner = $partner->Get($idPartner);
			if($partner->partnerId > 0) {
				$compa = new usuario();
				$us1 = $partner->idUsuario1;
				$us2 = $partner->idUsuario2;
				$idCompa = $this->usuarioId == $us1 ? $us2:$us1;
				$compa->Get($idCompa);
				$compa->voy = 1;
				$compa->Save();
				$this->compartirCon($idCompa);
			}
		} else {
			// Si no viene idPartner, se rompe la pareja de desayuno
			$this->dejarDeCompartir();
		}
		$this->voy = 1;
		$this->Save();

		return 1;
	}

	function dejarDeCompartir() {
		$partner = new partner();
		$partner = $partner->Get($this->actualPartnerId);
		$compa = $this->getCompa();
		if($compa->usuarioId > 0) {
			$compa->actualPartnerId = 0;
			$compa->Save();
		}

		$this->actualPartnerId=0;
		$this->Save();
	}

	function compartirCon($idCompa) {
		// Recuperamos al compañero
		$usuarioCompa = new usuario();
		$usuarioCompa = $usuarioCompa->Get($idCompa);

		if($idCompa != $usuarioCompa->usuarioId) {
			// Comprobamos que no existe una fila en la tabla partner en la que ya haya compartido con el el desayuno antes
			$partner = new partner();
			$fvc_array1 = array(array(partner::$fieldIdUsuario1, "=", $idCompa), array(partner::$fieldIdUsuario2, "=", $this->usuarioId));
			$fvc_array2 = array(array(partner::$fieldIdUsuario1, "=", $this->usuarioId), array(partner::$fieldIdUsuario2, "=", $idCompa));
			$partner = $partner->GetList($fvc_array1);
			if(count($partner) <= 0) {
				$partner = new partner();
				$partner = $partner->GetList($fvc_array2);
			}

			if(count($partner) <= 0) {
				// No existe esta pareja, la creamos
				$partner = new partner();
				$partner->idUsuario1 = $this->usuarioId;
				$partner->idUsuario2 = $idCompa;
				$partner->idUsuarioUltimoPagar = $this->usuarioId;
				$partner->Save();
			} else {
				$partner = $partner[0];
			}
			$this->actualPartnerId=$partner->partnerId;
			$this->Save();

			$usuarioCompa->actualPartnerId=$partner->partnerId;
			// Seteamos el desayuno del compañero para que coincida con el del usuario actual:
			$usuarioCompa->desayunoComida = $this->desayunoComida;
			$usuarioCompa->Save();
		}
	}

	function updateGrupo($idGrupo) {
		if(isset($idGrupo)) {
			$this->grupoId = $idGrupo;
			$this->Save();
			return $this;
		} else {
			return 0;
			exit("Error en updateGrupo, no se ha pasado ningun idGrupo");
		}
	}

	function toString() {
		$ret["usuarioId"] = $this->usuarioId;
		$ret["nombre"] = $this->nombre;
		$ret["apellidos"] = $this->apellidos;
		$ret["username"] = $this->username;
		$ret["email"] = $this->email;
		$ret["actualPartnerId"] = $this->actualPartnerId;
		$ret["desayunoComida"] = $this->desayunoComida;
		$ret["desayunoBebida"] = $this->desayunoBebida;
		$ret["voy"] = $this->voy;
		return $ret;
	}

	static function getUserFromToken($token) {
		$usuario = new usuario();
		$fvc_array = array(array(usuario::$fieldLoginToken, "=", $token));
		return $usuario->GetList($fvc_array);
	}

	static function resetVoy() {
		$allUsers = new usuario();
		$allUsers = $allUsers->GetList();
		$partnersIds = array();
		foreach($allUsers AS $user) {
			if($user->voy == 1 && $user->actualPartnerId > 0 && !in_array($user->actualPartnerId, $partnersIds)) {
				$partner = new partner();
				$partner = $partner->Get($user->actualPartnerId);
				$partner->changeUltimoEnPagar();
			}
			$user->voy = 0;
			$user->Save();
		}
	}

}
?>