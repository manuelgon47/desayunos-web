<?php
/**
* @author Manuel González Villegas
*/
include_once('base/class.partnerBase.php');
class partner extends partnerBase {

	static $fieldPartnerId = "partnerid";
	static $fieldIdUsuario1 = "idusuario1";
	static $fieldIdUsuario2 = "idusuario2";
	static $fieldIdUsuarioUltimoPagar = "idusuarioultimopagar";

	function changeUltimoEnPagar() {
		$this->idUsuarioUltimoPagar = $this->idUsuarioUltimoPagar == $this->idUsuario1 ? $this->idUsuario2:$this->idUsuario1;
		$this->Save();
	}

}
?>