<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `partner` (
	`partnerid` int(11) NOT NULL auto_increment,
	`idusuario1` INT NOT NULL,
	`idusuario2` INT NOT NULL,
	`idusuarioultimopagar` INT NOT NULL, PRIMARY KEY  (`partnerid`)) ENGINE=MyISAM;
*/

/**
* <b>partner</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.2 / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=partner&attributeList=array+%28%0A++0+%3D%3E+%27idUsuario1%27%2C%0A++1+%3D%3E+%27idUsuario2%27%2C%0A++2+%3D%3E+%27idUsuarioUltimoPagar%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27INT%27%2C%0A++1+%3D%3E+%27INT%27%2C%0A++2+%3D%3E+%27INT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class partnerBase extends POG_Base
{
	public $partnerId = '';

	/**
	 * @var INT
	 */
	public $idUsuario1;
	
	/**
	 * @var INT
	 */
	public $idUsuario2;
	
	/**
	 * @var INT
	 */
	public $idUsuarioUltimoPagar;
	
	public $pog_attribute_type = array(
		"partnerId" => array('db_attributes' => array("NUMERIC", "INT")),
		"idUsuario1" => array('db_attributes' => array("NUMERIC", "INT")),
		"idUsuario2" => array('db_attributes' => array("NUMERIC", "INT")),
		"idUsuarioUltimoPagar" => array('db_attributes' => array("NUMERIC", "INT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function partnerBase($idUsuario1='', $idUsuario2='', $idUsuarioUltimoPagar='')
	{
		$this->idUsuario1 = $idUsuario1;
		$this->idUsuario2 = $idUsuario2;
		$this->idUsuarioUltimoPagar = $idUsuarioUltimoPagar;
	}
	
	
	/**
	* Gets object from database
	* @param integer $partnerId 
	* @return object $partner
	*/
	function Get($partnerId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `partner` where `partnerid`='".intval($partnerId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->partnerId = $row['partnerid'];
			$this->idUsuario1 = $this->Unescape($row['idusuario1']);
			$this->idUsuario2 = $this->Unescape($row['idusuario2']);
			$this->idUsuarioUltimoPagar = $this->Unescape($row['idusuarioultimopagar']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $partnerList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `partner` ";
		$partnerList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "partnerid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$partner = new $thisObjectName();
			$partner->partnerId = $row['partnerid'];
			$partner->idUsuario1 = $this->Unescape($row['idusuario1']);
			$partner->idUsuario2 = $this->Unescape($row['idusuario2']);
			$partner->idUsuarioUltimoPagar = $this->Unescape($row['idusuarioultimopagar']);
			$partnerList[] = $partner;
		}
		return $partnerList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $partnerId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$rows = 0;
		if ($this->partnerId!=''){
			$this->pog_query = "select `partnerid` from `partner` where `partnerid`='".$this->partnerId."' LIMIT 1";
			$rows = Database::Query($this->pog_query, $connection);
		}
		if ($rows > 0)
		{
			$this->pog_query = "update `partner` set 
			`idusuario1`='".$this->Escape($this->idUsuario1)."', 
			`idusuario2`='".$this->Escape($this->idUsuario2)."', 
			`idusuarioultimopagar`='".$this->Escape($this->idUsuarioUltimoPagar)."' where `partnerid`='".$this->partnerId."'";
		}
		else
		{
			$this->pog_query = "insert into `partner` (`idusuario1`, `idusuario2`, `idusuarioultimopagar` ) values (
			'".$this->Escape($this->idUsuario1)."', 
			'".$this->Escape($this->idUsuario2)."', 
			'".$this->Escape($this->idUsuarioUltimoPagar)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->partnerId == "")
		{
			$this->partnerId = $insertId;
		}
		return $this->partnerId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $partnerId
	*/
	function SaveNew()
	{
		$this->partnerId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `partner` where `partnerid`='".$this->partnerId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `partner` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>