<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `grupo` (
	`grupoid` int(11) NOT NULL auto_increment,
	`nombre` VARCHAR(255) NOT NULL,
	`fechaultimoresumen` INT NOT NULL,
	`admingrupo` VARCHAR(255) NOT NULL, PRIMARY KEY  (`grupoid`)) ENGINE=MyISAM;
*/

/**
* <b>grupo</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.2 / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=grupo&attributeList=array+%28%0A++0+%3D%3E+%27nombre%27%2C%0A++1+%3D%3E+%27fechaultimoresumen%27%2C%0A++2+%3D%3E+%27admingrupo%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++1+%3D%3E+%27INT%27%2C%0A++2+%3D%3E+%27VARCHAR%28255%29%27%2C%0A%29
*/
include_once('class.pog_base.php');
class grupoBase extends POG_Base
{
	public $grupoId = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $nombre;
	
	/**
	 * @var INT
	 */
	public $fechaultimoresumen;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $admingrupo;
	
	public $pog_attribute_type = array(
		"grupoId" => array('db_attributes' => array("NUMERIC", "INT")),
		"nombre" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"fechaultimoresumen" => array('db_attributes' => array("NUMERIC", "INT")),
		"admingrupo" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function grupoBase($nombre='', $fechaultimoresumen='', $admingrupo='')
	{
		$this->nombre = $nombre;
		$this->fechaultimoresumen = $fechaultimoresumen;
		$this->admingrupo = $admingrupo;
	}
	
	
	/**
	* Gets object from database
	* @param integer $grupoId 
	* @return object $grupo
	*/
	function Get($grupoId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `grupo` where `grupoid`='".intval($grupoId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->grupoId = $row['grupoid'];
			$this->nombre = $this->Unescape($row['nombre']);
			$this->fechaultimoresumen = $this->Unescape($row['fechaultimoresumen']);
			$this->admingrupo = $this->Unescape($row['admingrupo']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $grupoList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `grupo` ";
		$grupoList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "grupoid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$grupo = new $thisObjectName();
			$grupo->grupoId = $row['grupoid'];
			$grupo->nombre = $this->Unescape($row['nombre']);
			$grupo->fechaultimoresumen = $this->Unescape($row['fechaultimoresumen']);
			$grupo->admingrupo = $this->Unescape($row['admingrupo']);
			$grupoList[] = $grupo;
		}
		return $grupoList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $grupoId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$rows = 0;
		if ($this->grupoId!=''){
			$this->pog_query = "select `grupoid` from `grupo` where `grupoid`='".$this->grupoId."' LIMIT 1";
			$rows = Database::Query($this->pog_query, $connection);
		}
		if ($rows > 0)
		{
			$this->pog_query = "update `grupo` set 
			`nombre`='".$this->Escape($this->nombre)."', 
			`fechaultimoresumen`='".$this->Escape($this->fechaultimoresumen)."', 
			`admingrupo`='".$this->Escape($this->admingrupo)."' where `grupoid`='".$this->grupoId."'";
		}
		else
		{
			$this->pog_query = "insert into `grupo` (`nombre`, `fechaultimoresumen`, `admingrupo` ) values (
			'".$this->Escape($this->nombre)."', 
			'".$this->Escape($this->fechaultimoresumen)."', 
			'".$this->Escape($this->admingrupo)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->grupoId == "")
		{
			$this->grupoId = $insertId;
		}
		return $this->grupoId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $grupoId
	*/
	function SaveNew()
	{
		$this->grupoId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `grupo` where `grupoid`='".$this->grupoId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `grupo` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>