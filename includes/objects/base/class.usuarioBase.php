<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `usuario` (
	`usuarioid` int(11) NOT NULL auto_increment,
	`nombre` VARCHAR(255) NOT NULL,
	`apellidos` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`desayunocomida` VARCHAR(255) NOT NULL,
	`desayunobebida` VARCHAR(255) NOT NULL,
	`username` VARCHAR(255) NOT NULL,
	`passwd` VARCHAR(255) NOT NULL,
	`grupoid` INT NOT NULL,
	`actualpartnerid` INT NOT NULL,
	`logintoken` VARCHAR(255) NOT NULL,
	`fechaultimoacceso` INT NOT NULL,
	`voy` INT NOT NULL, PRIMARY KEY  (`usuarioid`)) ENGINE=MyISAM;
*/

/**
* <b>usuario</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.2 / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=usuario&attributeList=array+%28%0A++0+%3D%3E+%27nombre%27%2C%0A++1+%3D%3E+%27apellidos%27%2C%0A++2+%3D%3E+%27email%27%2C%0A++3+%3D%3E+%27desayunoComida%27%2C%0A++4+%3D%3E+%27desayunoBebida%27%2C%0A++5+%3D%3E+%27username%27%2C%0A++6+%3D%3E+%27passwd%27%2C%0A++7+%3D%3E+%27grupoId%27%2C%0A++8+%3D%3E+%27actualPartnerId%27%2C%0A++9+%3D%3E+%27loginToken%27%2C%0A++10+%3D%3E+%27fechaUltimoAcceso%27%2C%0A++11+%3D%3E+%27voy%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++1+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++2+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++3+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++4+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++5+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++6+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++7+%3D%3E+%27INT%27%2C%0A++8+%3D%3E+%27INT%27%2C%0A++9+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++10+%3D%3E+%27INT%27%2C%0A++11+%3D%3E+%27INT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class usuarioBase extends POG_Base
{
	public $usuarioId = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $nombre;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $apellidos;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $email;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $desayunoComida;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $desayunoBebida;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $username;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $passwd;
	
	/**
	 * @var INT
	 */
	public $grupoId;
	
	/**
	 * @var INT
	 */
	public $actualPartnerId;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $loginToken;
	
	/**
	 * @var INT
	 */
	public $fechaUltimoAcceso;
	
	/**
	 * @var INT
	 */
	public $voy;
	
	public $pog_attribute_type = array(
		"usuarioId" => array('db_attributes' => array("NUMERIC", "INT")),
		"nombre" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"apellidos" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"email" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"desayunoComida" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"desayunoBebida" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"username" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"passwd" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"grupoId" => array('db_attributes' => array("NUMERIC", "INT")),
		"actualPartnerId" => array('db_attributes' => array("NUMERIC", "INT")),
		"loginToken" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"fechaUltimoAcceso" => array('db_attributes' => array("NUMERIC", "INT")),
		"voy" => array('db_attributes' => array("NUMERIC", "INT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function usuarioBase($nombre='', $apellidos='', $email='', $desayunoComida='', $desayunoBebida='', $username='', $passwd='', $grupoId='', $actualPartnerId='', $loginToken='', $fechaUltimoAcceso='', $voy='')
	{
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->email = $email;
		$this->desayunoComida = $desayunoComida;
		$this->desayunoBebida = $desayunoBebida;
		$this->username = $username;
		$this->passwd = $passwd;
		$this->grupoId = $grupoId;
		$this->actualPartnerId = $actualPartnerId;
		$this->loginToken = $loginToken;
		$this->fechaUltimoAcceso = $fechaUltimoAcceso;
		$this->voy = $voy;
	}
	
	
	/**
	* Gets object from database
	* @param integer $usuarioId 
	* @return object $usuario
	*/
	function Get($usuarioId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `usuario` where `usuarioid`='".intval($usuarioId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->usuarioId = $row['usuarioid'];
			$this->nombre = $this->Unescape($row['nombre']);
			$this->apellidos = $this->Unescape($row['apellidos']);
			$this->email = $this->Unescape($row['email']);
			$this->desayunoComida = $this->Unescape($row['desayunocomida']);
			$this->desayunoBebida = $this->Unescape($row['desayunobebida']);
			$this->username = $this->Unescape($row['username']);
			$this->passwd = $this->Unescape($row['passwd']);
			$this->grupoId = $this->Unescape($row['grupoid']);
			$this->actualPartnerId = $this->Unescape($row['actualpartnerid']);
			$this->loginToken = $this->Unescape($row['logintoken']);
			$this->fechaUltimoAcceso = $this->Unescape($row['fechaultimoacceso']);
			$this->voy = $this->Unescape($row['voy']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $usuarioList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `usuario` ";
		$usuarioList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "usuarioid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$usuario = new $thisObjectName();
			$usuario->usuarioId = $row['usuarioid'];
			$usuario->nombre = $this->Unescape($row['nombre']);
			$usuario->apellidos = $this->Unescape($row['apellidos']);
			$usuario->email = $this->Unescape($row['email']);
			$usuario->desayunoComida = $this->Unescape($row['desayunocomida']);
			$usuario->desayunoBebida = $this->Unescape($row['desayunobebida']);
			$usuario->username = $this->Unescape($row['username']);
			$usuario->passwd = $this->Unescape($row['passwd']);
			$usuario->grupoId = $this->Unescape($row['grupoid']);
			$usuario->actualPartnerId = $this->Unescape($row['actualpartnerid']);
			$usuario->loginToken = $this->Unescape($row['logintoken']);
			$usuario->fechaUltimoAcceso = $this->Unescape($row['fechaultimoacceso']);
			$usuario->voy = $this->Unescape($row['voy']);
			$usuarioList[] = $usuario;
		}
		return $usuarioList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $usuarioId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$rows = 0;
		if ($this->usuarioId!=''){
			$this->pog_query = "select `usuarioid` from `usuario` where `usuarioid`='".$this->usuarioId."' LIMIT 1";
			$rows = Database::Query($this->pog_query, $connection);
		}
		if ($rows > 0)
		{
			$this->pog_query = "update `usuario` set 
			`nombre`='".$this->Escape($this->nombre)."', 
			`apellidos`='".$this->Escape($this->apellidos)."', 
			`email`='".$this->Escape($this->email)."', 
			`desayunocomida`='".$this->Escape($this->desayunoComida)."', 
			`desayunobebida`='".$this->Escape($this->desayunoBebida)."', 
			`username`='".$this->Escape($this->username)."', 
			`passwd`='".$this->Escape($this->passwd)."', 
			`grupoid`='".$this->Escape($this->grupoId)."', 
			`actualpartnerid`='".$this->Escape($this->actualPartnerId)."', 
			`logintoken`='".$this->Escape($this->loginToken)."', 
			`fechaultimoacceso`='".$this->Escape($this->fechaUltimoAcceso)."', 
			`voy`='".$this->Escape($this->voy)."' where `usuarioid`='".$this->usuarioId."'";
		}
		else
		{
			$this->pog_query = "insert into `usuario` (`nombre`, `apellidos`, `email`, `desayunocomida`, `desayunobebida`, `username`, `passwd`, `grupoid`, `actualpartnerid`, `logintoken`, `fechaultimoacceso`, `voy` ) values (
			'".$this->Escape($this->nombre)."', 
			'".$this->Escape($this->apellidos)."', 
			'".$this->Escape($this->email)."', 
			'".$this->Escape($this->desayunoComida)."', 
			'".$this->Escape($this->desayunoBebida)."', 
			'".$this->Escape($this->username)."', 
			'".$this->Escape($this->passwd)."', 
			'".$this->Escape($this->grupoId)."', 
			'".$this->Escape($this->actualPartnerId)."', 
			'".$this->Escape($this->loginToken)."', 
			'".$this->Escape($this->fechaUltimoAcceso)."', 
			'".$this->Escape($this->voy)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->usuarioId == "")
		{
			$this->usuarioId = $insertId;
		}
		return $this->usuarioId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $usuarioId
	*/
	function SaveNew()
	{
		$this->usuarioId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `usuario` where `usuarioid`='".$this->usuarioId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `usuario` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>