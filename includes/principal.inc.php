<?
include("includes.inc.php");

function imprimeCabecera() {
	global $configuration;
	header('Content-Type: text/html; charset=utf-8');
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Desayunos</title>
	<!-- Styles -->
	<link rel="stylesheet" href="../includes/styles/css/general.css">
</head>
<body>
	<div class="menuTop">
	<?if(!empty($_SESSION["username"])) {?>
		<a class="logout" href=<?=$configuration["homepage"]."/includes/generalController.php?action=logout"?>>Logout</a>
	</div>
	<?
	}
}

function imprimePie() {
	?>
</body>
</html>
	<?
}

function logout() {
	global $configuration;
	$_SESSION = array();
	session_destroy();
	header("Location: ".$configuration["homepage"]);
	die();
}

function getCurrentUsuario() {
	$username = $_SESSION["username"];
	$usuario = new usuario();
	$fvc_array = array(array("username", "=", $username));
	$usuario = $usuario->GetList($fvc_array);
	if(count($usuario)<=0) {
		exit("No se ha recuperado ningún usuario");
	}
	$usuario = $usuario[0];
	if(!empty($usuario->usuarioId)){
		return $usuario;
	} else {
		exit("Error al recuperar al usuario");
	}
}

function getGrupo($idGrupo = "") {
	if($idGrupo == "") {
		$idGrupo = getCurrentUsuario()->grupoId;
	}
	if(!empty($idGrupo)) {
		$grupo = new Grupo();
		$grupo = $grupo->Get($idGrupo);
		return $grupo;
	}
}

?>
