<?php
include("../includes/principal.inc.php");
imprimeCabecera();

// Si el usuario no está autenticado lo mandamos al login
// TODO: Crear método e introducor en la cabecera menos en la pantalla de login
$username = $_SESSION["username"];
if(empty($username)) {
	global $configuration;
	header("Location: ".$configuration["homepage"]);
}

$currentUser = getCurrentUsuario();
$desayunosUser = $currentUser->getAllBreakfastGroup();

$resumen = $currentUser->getResumenDesayunos();
$bebidas = $resumen[0];
$totadas = $resumen[1];
?>
<h3>Desayuno de cada uno:</h3>
<table border="1">
	<tr>
		<th>Nombre</th><th>Desayuno Comida</th><th>Desayuno Bebida</th>
	</tr>
	<? foreach ($desayunosUser as $user) { ?>
		<tr>
			<td><?=$user->nombre?></td><td><?=$user->desayunoComida?></td><td><?=$user->desayunoBebida?></td>
		</tr>
	<? } ?>
</table>

<h3>Resumen:</h3>
<h5>Bebidas</h5>
<ul>
	<? foreach ($bebidas as $bebida) { ?>
		<li><?=$bebida?></li>
	<? } ?>
</ul>

<h5>Tostadas</h5>
<ul>
	<? foreach ($totadas as $tostada) { ?>
		<li><?=$tostada?></li>
	<? } ?>
</ul>

<?php
imprimePie();
?>