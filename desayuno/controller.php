<?php
include("../includes/principal.inc.php");

$action = $_POST["action"];
if(empty($action)) {
	$action = $_GET["action"];
}

if($action == "updateGrupo") {
	actionUpdateGrupo();
	header('Location: ' . $_SERVER['HTTP_REFERER'] . "?info=1");
} else if($action == "dejarCompartir") {
	actionDejarCompartir();
	header('Location: ' . $_SERVER['HTTP_REFERER'] . "?info=1");
} else if($action == "compartir") {
	actionCompartir();
	header('Location: ' . $_SERVER['HTTP_REFERER'] . "?info=1");
} else if($action == "updateDesayuno") {
	actionUpdateDesayuno();
	header('Location: ' . $_SERVER['HTTP_REFERER'] . "?info=1");
}

function actionUpdateGrupo() {
	$idGrupo = $_POST["grupos"];
	$currentUser = getCurrentUsuario();
	$currentUser->grupoId = $idGrupo;
	$currentUser->Save();
}

function actionDejarCompartir() {
	$currentUser = getCurrentUsuario();

	// Eliminamos el partner id a los dos; primero al compañero:
	$partner = new partner();
	$partner = $partner->Get($currentUser->actualPartnerId);
	$compa = $currentUser->getCompa();
	if($compa->usuarioId > 0) {
		$compa->actualPartnerId = 0;
		$compa->Save();
	}

	$currentUser->actualPartnerId=0;
	$currentUser->Save();
}

function actionCompartir() {
	// Recuperamos el id del usuario con el que queremos compartir:
	$idCompa = $_POST["idCompa"];
	$usuarioCompa = new usuario();
	$usuarioCompa = $usuarioCompa->Get($idCompa);

	// Current user:
	$currentUser = getCurrentUsuario();

	if(empty($usuarioCompa->usuarioId)) {
		exit("El id de usuario $idCompa no coincide con ningún id de usuario");
	}

	// Comprobamos que no existe una fila en la tabla partner en la que ya haya compartido con el el desayuno antes
	$partner = new partner();
	$fvc_array1 = array(array(partner::$fieldIdUsuario1, "=", $idCompa), array(partner::$fieldIdUsuario2, "=", $currentUser->usuarioId));
	$fvc_array2 = array(array(partner::$fieldIdUsuario1, "=", $currentUser->usuarioId), array(partner::$fieldIdUsuario2, "=", $idCompa));
	$partner = $partner->GetList($fvc_array1);
	if(count($partner) <= 0) {
		$partner = new partner();
		$partner = $partner->GetList($fvc_array2);
		//print_r("<pre>".print_r($partner)."</pre>");exit();
	}

	if(count($partner) <= 0) {
		// No existe esta pareja, la creamos
		$partner = new partner();
		$partner->idUsuario1 = $currentUser->usuarioId;
		$partner->idUsuario2 = $idCompa;
		$partner->idUsuarioUltimoPagar = $currentUser->usuarioId;
		$partner->Save();
	} else {
		$partner = $partner[0];
	}
	// Si no existe la creamos 
	// Si ya existe, simplemente metemos el id en nuestro actualPartnerId y en el de nuestro compañero
	$currentUser->actualPartnerId=$partner->partnerId;
	$currentUser->Save();

	$usuarioCompa->actualPartnerId=$partner->partnerId;
	// Seteamos el desayuno del compañero para que coincida con el del usuario actual:
	$usuarioCompa->desayunoComida = $currentUser->desayunoComida;
	$usuarioCompa->Save();
}

function actionUpdateDesayuno() {
	$desayunoComida = $_POST["desayunoComida"];
	$desayunoBebida = $_POST["desayunoBebida"];
	$currentUser = getCurrentUsuario();
	$currentUser->desayunoComida = $desayunoComida;
	$currentUser->desayunoBebida = $desayunoBebida;
	$currentUser->Save();

	// TODO: Actualizar la comida del compañero
	if($currentUser->actualPartnerId > 0) {
		$compa = $currentUser->getCompa();
		if($compa->usuarioId > 0) {
			$compa->desayunoComida = $desayunoComida;
			$compa->Save();
		}
	}
}

?>