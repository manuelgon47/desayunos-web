<?php
include("../includes/principal.inc.php");
imprimeCabecera();

// Si el usuario no está autenticado lo mandamos al login
// TODO: Crear método e introducor en la cabecera menos en la pantalla de login
$username = $_SESSION["username"];
if(empty($username)) {
	global $configuration;
	header("Location: ".$configuration["homepage"]);
}

//Obtenemos el usuario autenticado
$curretUsuario = getCurrentUsuario();

//Obtenemos todos los grupos de desayunos creados
$gruposDesayuno = new grupo();
$gruposDesayuno = $gruposDesayuno->GetList();


if(!empty($curretUsuario->grupoId)) {
	// Recuperamos los compañeros del grupo:
	$partners = $curretUsuario->getMyPartnersGroup();

	$grupo = getGrupo();
?>

<h3>Mi Grupo</h3>
<p><?=$grupo->nombre?></p>

<h3>Compañeros de grupo</h3>
<? if(count($partners) > 0) { ?>
	<table border="1">
		<tr>
			<td>Nombre</td><td>Desayuno Comida</td><td>Desayuno Bebida</td>
		</tr>
	<? foreach ($partners as $partner) { ?>
		<tr>
			<td><?= $partner->nombre ?></td><td><?= $partner->desayunoComida ?></td><td><?= $partner->desayunoBebida ?></td>
		</tr>
	<? } ?>
	</table>
	
<? } ?>

<h3>Mi Desayuno</h3>

<form action="controller.php" method="post">
	<input type="hidden" name="action" value="updateDesayuno">
	Comida:
	<br />
	<input name="desayunoComida" type="text" value="<?=$curretUsuario->desayunoComida?>" />
	<br />
	Bebida:
	<br />
	<input name="desayunoBebida" type="text" value="<?=$curretUsuario->desayunoBebida?>" />
	<br />
    <input type="submit" value="Gruardar" class="button" />
</form>


<h3>Compañero de desayuno actual</h3>
<!-- TODO: -->
<? if($curretUsuario->actualPartnerId > 0) { ?>
	<? $compa = $curretUsuario->getBreakfastPartner(); ?>
	<!-- Si tengo compañero de desayuno, aparece el nombre de mi compañero y la opcion dejar de compartir desayuno con fulano -->
	<p> Tu compañero de desayuno es <strong><em><?=$compa->nombre?></em></strong>. Si lo deseas puedes dejar de compartir desayuno con <em><?=$compa->nombre?> <a href="controller.php?action=dejarCompartir">pulsando aquí.</a></p>
<? } else { ?>

<p>No tienes ningún compañero de desayuno. Si lo deseas elige uno:</p>

<form action="controller.php" method="post">
	<input type="hidden" name="action" value="compartir" />
	<select class="compa" name="idCompa">
		<? foreach ($partners as $p) { ?>
			<option value=<?=$p->usuarioId?> selected="SELECTED"><?=$p->nombre . " " . $p->apellidos ?></option>	
		<? } ?>
	</select>
	<input type="submit" value="Unirme">
</form>
<br />
<? } ?>

<a href="desayunos.php">Obtener listado desayuno</a>

<!-- ++++++++++++++++++++++++++++ -->

<hr>

<p>Puedes cambiar de grupo de desayuno:</p>

<?} else {?>

<p>Aún no tienes ningún grupo de desayuno. Apuntante a uno de los existenes.</p>

<?}?>

<form action="controller.php" method="post">
	<input type="hidden" name="action" value="updateGrupo" />
	<select class="grupos" name="grupos">
	<?	foreach ($gruposDesayuno as $grupo) {
			if($curretUsuario->grupoId != $grupo->grupoId) {?>
				<option value=<?=$grupo->grupoId?> selected="SELECTED"><?=$grupo->nombre?></option>	
		<?	}
		}?>
	</select>
	<input type="submit" value="Unirme">
</form>

<?php
imprimePie();
?>