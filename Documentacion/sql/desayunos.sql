DROP DATABASE IF EXISTS Desayunos;
CREATE DATABASE Desayunos;

use Desayunos;

DROP TABLE IF EXISTS Usuarios;
CREATE TABLE Usuarios (
	idUsuario int NOT NULL AUTO_INCREMENT,
	nombre varchar(255) NOT NULL,
	username varchar(255) NOT NULL,
	passwd varchar(255) NOT NULL,
	idGrupo int,
	PRIMARY KEY(idUsuario)
);
INSERT INTO Usuarios (nombre, username, passwd) VALUES('Manuel Gonzalez', 'admin', '123456');

DROP TABLE IF EXISTS Grupos;
CREATE TABLE Grupos (
	idGrupo int NOT NULL AUTO_INCREMENT,
	nombreGrupo varchar(255) NOT NULL,
	PRIMARY KEY(idGrupo)
);

DROP TABLE IF EXISTS Desayunos;
CREATE TABLE Desayunos (
	idDesayuno int NOT NULL AUTO_INCREMENT,
	desayuno varchar(255) NOT NULL,
	PRIMARY KEY(idDesayuno)
);